# Add Test Data and Content
The Actor determines the [Test Data](/documentation/test-data) and [Content](/documentation/actor-api-content) that it needs. If a default value is provided, the  Test Data/Content will be optional; otherwise, it will be mandatory.

## Test Data

The basic version of [Test Data](/documentation/test-data) is a key-value pair. It is up to the Actor to cast the value to the correct type.

```tc
|TestDataTestCase                     |
|key        |value        |description|
|Request-URI|/denmark|           |
|Accept     |image/png    |           |
```

```javascript

*data() {
  this.requistUri = this.getTestDataString('Request-URI', this.requistUri);
  this.accept = this.getTestDataString('Accept', this.accept);
}

```

## Content
[Content](/documentation/actor-api-content) is chosen by name. The Actor chooses whether the name is hard-coded or retrieved from [Test Data](/documentation/test-data).

```tc
|TestDataTestCase                   |
|key       |value       |description|
|image_name|flag_denmark|           |
```

```javascript

*data() {
  this.content = this.getContent('image_name', 'image');
  this.contentByName = this.getContentByName('flag_denmark', 'image');
}

```
### Implement in:
TC: [Image-Workshop](/test-cases/ImageService/Image/Image-Workshop/definition)

### Example
TC: [ImagePng-2-Test-Data-Denmark](/test-cases/ImageService/Image/ImagePng-2-Test-Data-Denmark)
TC: [ImagePng-2-Test-Data-Sweden](/test-cases/ImageService/Image/ImagePng-2-Test-Data-Sweden)
