# Introduction - Stack
## What is a Stack?
A Stack is a group of components used to send and receive messages from an Actor. It is an implementation of a protocol - either a standard or proprietary protocol. The Stack abstraction differs slightly from those commonly used when implementing protocols; it contains only an Encoder and Decoder, and does not keep states or handle timers. These parts of a common stack will be implemented in Actors when needed.

The components of a Stack are:
* **Connection** - sends and receives messages
* **Message** - a container of data from which data can be sent and to which data can be received
* **Encoder** - encodes the data of a message to the chosen protocol
* **Decoder** - decodes the selected protocol data to a message

Some common stacks include: **Http**, **Diameter**, **Radius**, **Sip**, **MQTT**, and others.
 
