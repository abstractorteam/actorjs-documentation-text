# SocketConnectionClient
* **[sendLine(msg)](#sendLine)**
* **[send(msg)](#send)**
* **[receiveLine()](#receiveLine)**
* **[receiveSize(size)](#receiveSize)**

### sendLine(msg)<a name="sendLine" class="markup_bookmark" href="#sendLine">#</a>
* msg [&lt;string&gt;]() The string to send on the socket. The connection will add '\r\n' to the string value.

```javascript

class MySocketActorOrig extends ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
  }

  *initClient() {
    this.socketConnection = this.createConnection('socket');
  }
  
  *run() {
    this.socketConnection.sendLine('My message');
  }

  *exit() {
    this.socketConnection.close();
  }
}
```
The string 'My message\r\n' will be send on the socket.

### send(msg)<a name="send" class="markup_bookmark" href="#send">#</a>
* msg [&lt;string&gt;]() | [&lt;Buffer&gt;]() The data to send on the socket.

```javascript

class MySocketActorOrig extends ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
  }

  *initClient() {
    this.socketConnection = this.createConnection('socket');
  }
  
  *run() {
    this.socketConnection.send('My message');
  }

  *exit() {
    this.socketConnection.close();
  }
}
```

The string 'My message' will be send on the socket.

### <span class="markup_command">yield</span> **receiveLine()**<a name="receiveLine" class="markup_bookmark" href="#receiveLine">#</a>
* Returns: [&lt;string&gt;]() The string sent sent on the socket until '\r\n' is found. '\r\n' will be removed from the string.

```javascript

class MySocketActorTerm extends ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
  }

  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
    let request = this.socketConnection.receiveLine();
    VERIFY_VALUE('My message', request);
  }

  *exit() {
    this.socketConnection.close();
  }
}
```

**<span class="markup_command">yield</span>** **receiveSize(size)**<a name="receiveSize" class="markup_bookmark" href="#receiveSize">#</a>
* size [&lt;number&gt;]() The number of octets to read.
* Returns: [&lt;Buffer&gt;]() The read octets.
