# Client Architecture
The Client is mainly built by two base components: [React Component](platform-client-react-component) and [Store](platform-client-store).

## Store Update
[React Components](platform-client-react-component) can inform [Stores](platform-client-store) that some thing has happened with Actions. An action can contain data that the [Store](platform-client-store) can use. If the Action changes the state of the [Store](platform-client-store) then the [React Components](platform-client-react-component) that are dependent on the [Store](platform-client-store) will be updated.

### Store Update - No ServerMaster Communication
```seq
Title: Store Update - No ServerMaster Communication
Nodes[ReactComponent1, ReactComponentN, Store1, StoreN, ServerMaster, ServerSlave]
ReactComponent1 -o Store1[unknown]: listen
ReactComponentN -o Store1[unknown]: listen
ReactComponentN -o StoreN[unknown]: listen
ReactComponent1 => Store1[unknown]: actionX
Store1 => ReactComponent1[unknown]: update
Store1 => ReactComponentN[unknown]: update
ReactComponentN -x Store1[unknown]: stop listen
ReactComponent1 => Store1[unknown]: actionX
Store1 => ReactComponent1[unknown]: update
```

### Store Update - ServerMaster Communication (HTTP)
```seq
Title: Store Update - ServerMaster Communication (HTTP)
Nodes[ReactComponent1, ReactComponentN, Store1, StoreN, ServerMaster, ServerSlave]
ReactComponent1 -o Store1[unknown]: listen
ReactComponent1 -o StoreN[unknown]: listen
ReactComponentN -o Store1[unknown]: listen
ReactComponentN -o StoreN[unknown]: listen
ReactComponent1 => StoreN[unknown]: actionX
StoreN => ServerMaster[http]: request
ServerMaster => StoreN[http]: response
StoreN => ReactComponent1[unknown]: update
StoreN => ReactComponentN[unknown]: update
ReactComponentN -x Store1[unknown]: stop listen
ReactComponent1 => Store1[unknown]: actionX
Store1 => ServerMaster[http]: request
ServerMaster => Store1[http]: response
Store1 => ReactComponent1[unknown]: update
```