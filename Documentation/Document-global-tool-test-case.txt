# Test Case Tool
The Test Case Tool is used to create, specify and run [Test Case](test-cases)s.

## Test Case Tool Tabs
+ [The Definition Tab](tool-test-case-definition)
+ [The Execution Tab](tool-test-case-execution)
+ [The Debug Tab](tool-test-case-debug)
+ [The Log Tab](tool-test-case-log)
+ [The Sequence Diagram Tab](tool-test-case-sequence-diagram)
+ [The Specification Tab](tool-test-case-specification)
+ [The Analyze Tab](tool-test-case-analyze)