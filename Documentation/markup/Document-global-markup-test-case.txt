<a name="topOfPage"></a>
# **Markup - Test Case**
Test Case Markup defines a [[REF=,ABS_Test_Case]] and it provides the documentation with test case images. 

[[EMBED=markup/embed/inner_markup_tc]]
