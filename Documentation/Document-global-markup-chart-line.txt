# Markup - Chart Line
```chart-line
Title: Demo title
Config(width: 600, height: 300, widthBias: 40, heightBias: 40)

Axis(x, 20)
Axis(y, 10)

Grid(1, 1)

Line1(blue)[0,1][1,5][2,5][3,3][4,9][5,8][6,9][7,10][20,9]
Line2(red)[0,0][1,7][2,2][3,9][20,10]
Line3(yellow)[0,0][20,4]
Line4(green)[0,0][20,8]

```
