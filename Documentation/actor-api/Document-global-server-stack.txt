# **Server Stack API**

[[NOTE={"guid":"f0f932f7-60a0-4bfe-9f79-6bfe6b623051"}]]
[[ANCHOR={"id":"97aa2f7b-182a-4530-bda1-e96694ce2500","visible":true}]]
## **Description**

By definition a server is an [[REF=, ABS_Actor]] allowing another parts called a clients to connect to it. When the [[REF=, ABS_Actor]] is connected it will get a [[REF=, ABS_Stack_Server_Connection]] to send and receive data through. A [[REF=, ABS_Stack_Server_Connection]] implements the stack part of a protocol and contains an Encoder and a Decoder. All kind of timing handling in the Stack will not be handled by the [[REF=, ABS_Stack_Server_Connection]] . It must be handled by the [[REF=, ABS_Actor]] it self. The transport layer is separated from the [[REF=, ABS_Stack]]  in the [[REF=, ABS_Stack_Server_Connection]] which makes it possible to use all implemented transports on all [[REF=, ABS_Stack]]s.

The following Actor types do have server capability.
+ **[[REF=, ABS_Actor_Terminating]]**
+ **[[REF=, ABS_Actor_Proxy]]**

In the ***initServer()** method and in the ***run()** method it's possible for an [[REF=, ABS_Actor]] to create a [[REF=, ABS_Stack_Server_Connection]]. 

The implementation of the Server Stack API starts in [[REF=, CODE_Stack_Server]].

[[NOTE={"guid":"511cb0c6-413e-4d9e-b89a-24d272866d55"}]]
[[ANCHOR={"id":"fbf4c7a3-28fa-4cd1-9545-0b1ab9bad220","visible":true}]]
## **API Methods**
* **[*createServer](#server-stack-api-create-server)**

***
[[NOTE={"guid":"e4743fcb-5743-4b75-ba2e-86eb21e3e3f4"}]]
[[ANCHOR={"id":"server-stack-api-create-server","visible":true}]]
### ***createServer**

``` js
this.createServer(stackName, options)
```

#### **Method Description**
Starts a server, if it is not already started, and returns a [[REF=, ABS_Stack_Server_Connection]].


[[NOTE]]
[[ANCHOR]]
```table
Config(classHeading: )

|Parameters                                                                                                                                                                                                                                                                                                                                         |
|Name                     |Type                |Default                  |Description                                                                                                                                                                                                                                                               |
|stackName                |[[REF=,MDN_string]] |                         |Name of the Server Stack.                                                                                                                                                                                                                                                 |
|options                  |[[REF=,MDN_Object]] |                         |Object with options                                                                                                                                                                                                                                                       |
|\- srvIndex              |[[REF=,MDN_number]] |0                        |Index of the srv column from the Actor table. If the srv column is a [[REF=,MDN_string]], the name of the destination address, the value must be 0. If the srv column is an Array [[REF=,MDN_strings]], the value must be an existing index in the Array.                 |
|\-&nbsp;networkType      |[[REF=,MDN_number]] |[determined by the Stack]|0: TCP, 1: UDP. Number defining the network Type. Implementations of stacks separate the data sent and the transport layer to be used. The stack determines the default value. See the help description for the stack whose name is used in the first parameter stackName.|
|\- reuse                 |[[REF=,MDN_boolean]]|false                    |`ToDo:`                                                                                                                                                                                                                                                                   |
|\- share                 |[[REF=,MDN_boolean]]|false                    |`ToDo:`                                                                                                                                                                                                                                                                   |
|\-&nbsp;connectionOptions|[[REF=,MDN_Object]] |                         |connectionOptions is a connection-specific object. See the help description for the stack you used to find out which methods are supported.                                                                                                                               |
```

[[NOTE={"guid":"a35f4891-fb2f-4550-b588-100f3a9b293b"}]]
[[ANCHOR={"id":"5058d149-3759-4bae-9936-6664bbb75027","visible":true}]]
```table
Config(classHeading: )

|Returns                                                                                                                                                                                         |
|Type               |Description                                                                                                                                                                 |
|[[REF=,MDN_Object]]|The returned value is an object that inherits from ***StackApi.ServerConnection***. See the help description for the stack you used to find out which methods are supported.|
```

[[NOTE={"guid":"c6118eb0-e62a-49df-a048-b4bcba24def3"}]]
[[ANCHOR={"id":"9841e6ce-11db-42ee-a65d-4a08f7d7335f","visible":true}]]
```table
Config(classHeading: )

|Async                                                                                 |
|type |Description                                                                     |
|yield|Naming convention generated. If regexp ***/this.(.\*)Connection/*** is fulfilled|
```


[[NOTE={"guid":"bd9cc25e-3de9-4670-ad28-fec1564c41f4"}]]
[[ANCHOR={"id":"52831e82-a631-449e-9b1c-552606b8e374","visible":true}]]
#### **Example using createServer - 1**
Normal usage of the Server Stack API

```tc
|Actor                                                                              |
|name              |type|phase|execution|src|dst     |srv     |testData|verification|
|SocketExample1Orig|orig|exec |         |   |testSrv1|        |        |            |
|SocketExample1Term|term|exec |         |   |        |testSrv1|        |            |
```

[[NOTE={"guid":"f27e316c-ba32-4117-a09b-2a59429243cf"}]]
[[ANCHOR={"id":"1f43ef25-9cfb-404b-8f15-5c5a99b68f05","visible":true}]]
```javascript

class SocketExample1Term extends ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
  }

  *initServer() {
    this.socketConnection = this.createServer('socket');
  }

  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

```
[[NOTE={"guid":"fe2d6c80-8578-48ed-928e-5a1c71a345e1"}]]
[[ANCHOR={"id":"15616995-ffdc-4f2b-a340-a8a5984cbb3e","visible":true}]]
#### **Example using createServer - 2**
Changing the networkType

```tc
|Actor                                                                                   |
|name              |type|phase|execution|src     |dst     |srv     |testData|verification|
|SocketExample2Orig|orig|exec |         |testSrc1|testSrv1|        |        |            |
|SocketExample2Term|term|exec |         |        |        |testSrv1|        |            |

|TestDataTestCase           |
|key      |value|description|
|transport|udp  |           |
```

[[NOTE={"guid":"31f69662-7a09-4134-b124-0b90020ca8d3"}]]
[[ANCHOR={"id":"4a740908-d8be-4c89-b768-e3083e7a8b85","visible":true}]]
```javascript

class SocketExample2Term extends ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
    this.networkType = 0;
  }

  *data() {
    this.networkType = this.getTestDataNumber('transport', 'tcp', {
      tcp: NetworkType.TCP,
      udp: NetworkType.UDP
    });
  }

  *initServer() {
    this.socketConnection = this.createServer('socket', {
      networkType: this.networkType
    });
  }

  *exit() {
    this.closeConnection(this.socketConnection);
  }
}

```

[[NOTE={"guid":"5ed6b29e-f25b-4c02-a61c-8d7178ae2839"}]]
[[ANCHOR={"id":"a4f76d72-f272-4a2d-b94a-aa2d355b6837","visible":true}]]
#### **Example using createServer - 3**
Creating two servers

```tc
|Actor                                                                                                                                     |
|name                |type|phase|execution|src                     |dst                     |srv                     |testData|verification|
|SocketExample3Orig	g|orig|exec |         |["testSrc1", "testSrc2"]|["testSrv1", "testSrv2"]|                        |        |            |
|SocketExample3Term  |term|exec |         |                        |                        |["testSrv1", "testSrv2"]|        |            |
```

[[NOTE={"guid":"c9fd4699-773c-4336-94b8-8e6db7dca59b"}]]
[[ANCHOR={"id":"ddbd52ba-4946-4d68-a3ae-98f8531fcb23","visible":true}]]
```javascript

class SocketExample3Term extends ActorTerminating {
  constructor() {
    super();
    this.socketConnection1 = null;
    this.socketConnection2 = null;
  }

  *initServer() {
    this.socketConnection1 = this.createServer('socket');
    this.socketConnection2 = this.createServer('socket', {
      srvIndex: 1
    });
  }

  *exit() {
    this.closeConnection(this.socketConnection1);
    this.closeConnection(this.socketConnection2);
  }
}

```

[[NOTE={"guid":"fc93b7b1-cd51-4616-857e-ef263f10809a"}]]
[[ANCHOR={"id":"f1abb1ce-a23d-47d5-be06-390649e636a7","visible":true}]]
#### **Test Cases using createServer**
+ [DocActorApiClientStackCreateConnectionSingle](/../test-cases/Actor/Doc/DocActorApiClientStackCreateConnectionSingle)
+ [DocActorApiClientStackCreateConnectionSingleUdp](/../test-cases/Actor/Doc/DocActorApiClientStackCreateConnectionSingleUdp)
+ [DocActorApiClientStackCreateConnectionDouble](/../test-cases/Actor/Doc/DocActorApiClientStackCreateConnectionDouble)

