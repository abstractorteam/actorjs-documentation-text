# The Execution Tab

The Execution tab shows the *execution result*, *start time* and *duration* of the Test Case.

[[EMBED=embedded/test-case-result]]

**time:** The time when the Test Case started.
* *format:* `weekday`, `day` `month` `year` `hour`:`minute`:`second` GMT : `milliseconds with three decimals`
* *example:* Mon, 03 Oct 2016 22:52:05 GMT : 901.764

**duration:** The duration of the Test Case.
* *format:* `milliseconds with three decimals`ms
* *example:* 0.570ms

## Toolbar
The **execution buttons** to execute the Test Case are:
<br/>

```object
[button](glyphicon-play test_case_execution, **Run:** Execute the Test Case.)
[br]
[button](glyphicon-play test_case_execution_slow, **Run Slow:** Execute the Test Case in a **visualized** way by a short delay between the different states of the [Actors](actors). The delay is set as [Test Data](test-datas). The default value is set in the [TestDataEnvironment](/test-data/environment) tables.)
[br]
[button](glyphicon-stop test_case_execution, **Stop:** Stop executing the Test Case.)
[br]
[button](glyphicon-erase test_case_execution, **Clear:** Remove all execution data.)
```